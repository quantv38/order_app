import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid black",
    backgroundColor: "#1d39c4",
    height: "7vh",
  },
  paper: {
    textAlign: "center",
    fontSize: "20px",
  },
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div>
      <Grid container className={classes.root}>
        <Grid item xs={4} className={classes.paper}>
          <Link
            to="/employee"
            style={{
              color: "white",
              fontWeight: "bold",
              textDecoration: "none",
              margin: "auto",
            }}
          >
            Nhân viên
          </Link>
        </Grid>
        <Grid item xs={4} className={classes.paper}>
          <Link
            to="/chef"
            style={{
              color: "white",
              fontWeight: "bold",
              textDecoration: "none",
            }}
          >
            Đầu bếp
          </Link>
        </Grid>
        <Grid item xs={4} className={classes.paper}>
          <Link
            style={{
              color: "white",
              fontWeight: "bold",
              textDecoration: "none",
            }}
          >
            Đăng nhập
          </Link>
        </Grid>
      </Grid>
    </div>
  );
}
