import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Checkbox from "@material-ui/core/Checkbox";
import Avatar from "@material-ui/core/Avatar";

//
import Header from "./Header";
import { Typography, Grid, Paper } from "@material-ui/core";
import { socket } from "../../App";


const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CheckboxListSecondary() {
  const classes = useStyles();
  const [checked, setChecked] = useState([]);

  const [item, setItem] = useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  useEffect(() => {
    socket.on("toChef", (data) => {
      setItem(item => [...item, data])
      console.log(data)
    });

    // return socket.disconnect()
  }, []);

  console.log('item', item)

  return (
    <Grid container>
      <Grid item xs={12}>
        <Header />
      </Grid>
      <Grid
        item
        xs={12}
        style={{
          margin: "10px 0",
        }}
      >
        <Paper elevation={3}>
          <Typography
            variant="h5"
            gutterBottom
            style={{ textAlign: "center", backgroundColor: "#bfbfbf" }}
          >
            Table
          </Typography>
          <List dense className={classes.root}>
            {item.map((value) => {
              const labelId = value.food_detail.id;
              return (
                <ListItem key={value} button>
                  <ListItemText id={labelId} primary={value.food_detail.name} />
                  <ListItemText id={labelId} primary={value.quantity} />
                  <ListItemSecondaryAction>
                    <Checkbox
                      edge="end"
                      onChange={handleToggle(value)}
                      checked={checked.indexOf(value) !== -1}
                      inputProps={{ "aria-labelledby": labelId }}
                    />
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}
          </List>
        </Paper>
      </Grid>
    </Grid>
  );
}
