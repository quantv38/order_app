import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

//
import Header from "./Header";
import PinnedSubheaderList from "../../components/PinnedSubheaderList.component";
import NativeSelects from "../../components/NativeSelects.component";

const ChefView = () => {
  const [food, setFood] = useState([
    {
      id: "1",
      name: "Mỳ tôm",
      price: 4000,
    },
    {
      id: "2",
      name: "Bún cá",
      price: 30000,
    },
    {
      id: "3",
      name: "Beefsteak",
      price: 100000,
    },
    {
      id: "4",
      name: "Burger",
      price: 60000,
    },
    {
      id: "5",
      name: "Bia",
      price: 15000,
    },
    {
      id: "6",
      name: "Rượu táo mèo",
      price: 50000,
    },
    {
      id: "7",
      name: "Coca",
      price: 18000,
    },
  ]);
  return (
    <div>
      <Header />
      <Grid container spacing={5}>
        <Grid item xs={6} container>
          <Grid item xs={12}>
            <FormControlLabel
              value="start"
              control={<Checkbox color="primary" />}
              label="Table 1"
              labelPlacement="start"
            />
          </Grid>
          <Grid item xs={12}>
            <PinnedSubheaderList data={food} checkbox={0} />
          </Grid>
        </Grid>
        <Grid item xs={6} container>
          <Grid item xs={12}>
            <FormControlLabel
              value="start"
              control={<Checkbox color="primary" />}
              label="Table 2"
              labelPlacement="start"
            />
          </Grid>
          <Grid item xs={12}>
            <PinnedSubheaderList data={food} checkbox={0} />
          </Grid>
        </Grid>
        <Grid item xs={6} container>
          <Grid item xs={12}>
            <FormControlLabel
              value="start"
              control={<Checkbox color="primary" />}
              label="Table 1"
              labelPlacement="start"
            />
          </Grid>
          <Grid item xs={12}>
            <PinnedSubheaderList data={food} checkbox={0} />
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default ChefView;
