import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import { socket } from "../../App";

//
import Header from "./Header";
import NativeSelects from "../../components/NativeSelects.component";
import PinnedSubheaderList from "../../components/PinnedSubheaderList.component";
// import FoodContext from "../../components/Context.component";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "5px",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function CenteredGrid(props) {
  const classes = useStyles();
  const [item, setItem] = useState([
    {
      id: "1",
      name: "Mỳ tôm",
      price: 4000,
    },
    {
      id: "2",
      name: "Bún cá",
      price: 30000,
    },
    {
      id: "3",
      name: "Beefsteak",
      price: 100000,
    },
    {
      id: "4",
      name: "Burger",
      price: 60000,
    },
    {
      id: "5",
      name: "Bia",
      price: 15000,
    },
    {
      id: "6",
      name: "Rượu táo mèo",
      price: 50000,
    },
    {
      id: "7",
      name: "Coca",
      price: 18000,
    },
  ]);
  const [order, setOrder] = useState([]);
  const [checked, setChecked] = useState([]);
  const [bill, setBill] = useState({
    table: null,
    food: [],
    total: null,
  });

  const [id, setId] = useState([]);

  //chọn item vào giỏ đồ
  const chooseItem = (food, value) => {
    //Chọn bàn trước
    if (bill.table === null) {
      alert("Làm ơn chọn bàn trước khi chọn món");
    } else {
      // const listOrder = order.indexOf(value);
      const listChecked = checked.indexOf(value);
      const newOrder = [...order];
      const newChecked = [...checked];

      if (listChecked === -1) {
        newOrder.push(food);
        newChecked.push(value);
      } else {
        newOrder.splice(listChecked, 1);
        newChecked.splice(listChecked, 1);
      }
      setOrder(newOrder);
      setChecked(newChecked);
    }
  };

  const sendToBill = () => {
    var totalBill = bill.total || 0;
    var otherItem = [];

    if (order.length === 0 || bill.table === null) {
      console.log("loiiiiiiiiiiiii");
    } else if (bill.food.length === 0) {
      console.log("length 0");
      const newBill = bill.food.concat(order);
      order.map((item) => {
        totalBill = totalBill + item.food_detail.price * item.quantity;
      });

      setBill({ ...bill, ["food"]: newBill, ["total"]: totalBill });
      setOrder([]);
      setChecked([]);
      socket.emit("send-bill", order);
    } else {
      const oldBill = bill.food;

      // console.log("newbill", bill.food);

      order.map((item) => {
        oldBill.map((food) => {
          if (food.food_detail.id == item.food_detail.id) {
            console.log("true");
            // oldBill[j].quantity += order[i].quantity;
            // console.log("food.food_detail.id", oldBill[j].food_detail.id);
            // console.log("order[i].food_detail.id", order[i].food_detail.id);
          }
          // else {
          //   console.log(otherItem.indexOf(item.food_detail.id));
          //   if (otherItem.indexOf(item.food_detail.id) === -1) {
          //     console.log("false");
          //     setId((id) => id.concat(item.food_detail.id));
          //   } else {
          //     console.log("trung id");
          //   }
          // }
        });
        totalBill = totalBill + item.food_detail.price * item.quantity;
      });

      console.log("other: ", id);
      var newBill = oldBill.concat(order);
      setBill({ ...bill, ["food"]: newBill, ["total"]: totalBill });
      setOrder([]);
      setChecked([]);
      socket.emit("send-bill", order);
    }
  };
  // console.log('bill: ',bill.food);

  const getTable = (e) => {
    setBill({ ...bill, ["table"]: e.target.value });
  };

  return (
    <div className={classes.root}>
      <Header />
      <Grid container style={{}} spacing={2}>
        <Grid item xs={12} style={{ padding: "10px 0" }}>
          <NativeSelects callback={getTable} />
        </Grid>
        <Grid item xs={4} style={{}}>
          <PinnedSubheaderList
            checkbox={0}
            data={item}
            itemOrder={order}
            listCheck={checked}
            callback={chooseItem}
          />
        </Grid>
        <Grid item xs={4} style={{}}>
          <PinnedSubheaderList
            checkbox={1}
            quantity={true}
            data={order}
            listCheck={checked}
            itemOrder={order}
          />
          <Button
            variant="contained"
            color="primary"
            style={{ width: "60%", marginLeft: "20%" }}
            onClick={() => sendToBill()}
          >
            Đặt món
          </Button>
        </Grid>
        <Grid item container direction="column" xs={4} style={{}}>
          <Grid item>
            <PinnedSubheaderList
              checkbox={1}
              quantity={false}
              data={bill.food}
              listCheck={checked}
              itemOrder={order}
            />
          </Grid>
          {bill.total !== null && (
            <Grid item>
              <Typography
                variant="h5"
                gutterBottom
                style={{ textAlign: "right", margin: "10px 0" }}
              >
                Total: ${bill.total}
              </Typography>
            </Grid>
          )}
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              style={{ width: "60%", marginLeft: "20%" }}
            >
              Thanh toán
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
