import React, { useEffect, useContext } from "react";
import ReactDOM from "react-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import io from "socket.io-client";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

//
import Employee from "./views/Employee/EmployeeView";
import Chef from "./views/Chef/ChefContainer";

//
export var socket = io("http://localhost:8088");;

function App(props) {
    useEffect(() => {
      // socket.on("chao", (data) => {
      // });

      // socket.on("toChef", (data) => {
      //   alert(data);
      // });
    }, []);

  return (
    <Router>
      <Container>
        <Switch>
          <Route path="/chef">
            <Chef />
          </Route>
          <Route path="/employee">
            <Employee />
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
